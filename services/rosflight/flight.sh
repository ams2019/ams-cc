#!/bin/bash

# LOG
LOG=/home/pi/ams-cc/logs/rosflight.log
truncate -s 0 $LOG

# SETUP
source /home/pi/catkin_ws/devel/setup.bash

# ROSflight
unbuffer roslaunch /home/pi/ams-cc/services/rosflight/rosflight.launch 2>&1 | tee $LOG
