#!/bin/bash

# LOG
LOG=/home/pi/ams-cc/logs/rosflight.log
MSG="Stopping LIDAR for IMU calibration"
echo $MSG >> $LOG

# ROS
source /opt/ros/kinetic/setup.bash
export ROS_MASTER_URI=http://localhost:11311
export ROS_HOSTNAME=localhost

# ROSflight
source /home/pi/catkin_ws/devel/setup.bash
echo $MSG
rosservice call stop_motor
rosservice call calibrate_imu
rosservice call start_motor
