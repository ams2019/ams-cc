#!/bin/bash

# LOG
LOG=/home/pi/ams-cc/logs/roscore.log
truncate -s 0 $LOG

# SETUP
source /home/pi/catkin_ws/devel/setup.bash
export ROS_MASTER_URI=http://localhost:11311
export ROS_HOSTNAME=localhost

# ROS
unbuffer roscore 2>&1 | tee $LOG