# AMS Companion Computer

This repository contains scripts and configurations for the companion computer.

## Requirements

### Update

Update the system and all packages before starting with the setup

    sudo apt-get update && sudo apt-get upgrade -y

### ROS

Depending on your operating system choose correct installer script located in `ams-cc/install` folder.

- for ubuntu 16.04 (xenial) use `ros_setup_kinetic.sh`
- for ubuntu 18.04 (bionic) use `ros_setup_melodic.sh`

### ROSflight

Depending on your ROS version choose correct `ros_release` to use in install command below.

- for ubuntu 16.04 (xenial) use `kinetic`
- for ubuntu 18.04 (bionic) use `melodic`

        sudo apt install ros-<ros_release>-rosflight

### Other Dependencies

- pip (python package manager)

        sudo apt install python-pip

- htop (system monitoring tool)

        sudo apt install htop

### Bash RC

In order to set all necessary environment variables on every login (debug connection or ssh), add contents of `ams-cc/npi/.bashrc` at the end of `~/.bashrc` on the NanoPi.

## Installation

Clone `ams-cc` into home directory

    cd ~
    git clone https://Rafael91@bitbucket.org/ams2019/ams-cc.git

## How to use

### Register as Linux-Service

To register the components as linux service, link the following files:  
(This adds a symlinks at `/etc/systemd/system`)

    sudo systemctl link ~/ams-cc/services/roscore/core.service
    sudo systemctl link ~/ams-cc/services/rosflight/flight.service

Start/Stop/Restart service with

    sudo service {service-name} {start/stop/restart}

After making changes on service-file restart `systemctl` with

    sudo systemctl daemon-reload

Then restart service with

    sudo service {service-name} restart

### Start

To start all services execute

    /home/pi/npi/autostart.sh

To stop all services execute

    /home/pi/npi/stop.sh

### Autostart

To launch the service on every start create following symlink (replace {x}):

    sudo ln -s ~/ams-cc/{x}pi/10-run-ros.sh /etc/profile.d/10-run-ros.sh

#### Logs

All services write logs to the `logs` folder. To get the combined output of all logfiles run the monitor script

    ~/ams-cc/logs/monitor.sh

##### Ubuntu 16.04

To enable logging on Ubuntu 16.04 install `unbuffer` which is part of `expect`

    sudo apt install expect

To read single log files use:

    sudo tail -f ~/ams-cc/logs/rosflight.log

##### Ubuntu 18.04

To read log files use:

    sudo tail -f ~/ams-cc/logs/access.log
    sudo tail -f ~/ams-cc/logs/error.log

## Backup

Backup the eMMC of the NanoPi Neo Air to a microSD card

### Prepare microSD card (OS X)

#### Install e2fsprogs

    brew install e2fsprogs

#### Format microSD card

- Check mounting point of microSD:

        diskutil list

- Format with e2fsprogs (Replace N)

        sudo $(brew --prefix e2fsprogs)/sbin/mkfs.ext3 /dev/diskN

### Perform Backup (NanoPi NEO Air)

#### Check mounting point (`/dev/mmcblk1`)

    sudo fdisk -l

#### Mount microSD card

    sudo mount /dev/mmcblk1 /mnt

#### Create Backup

    sudo dd if=/dev/mmcblk0 of=/mnt/YYYYMMDD_backup_emmc.img bs=100M status=progress

#### Unmount microSD card

    sudo umount /dev/mmcblk1