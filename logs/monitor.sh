#!/bin/bash

CORE=/home/pi/ams-cc/logs/roscore.log
FLIGHT=/home/pi/ams-cc/logs/rosflight.log
LIDAR=/home/pi/ams-sens/logs/rplidar.log
SONIC=/home/pi/ams-sens/logs/ultrasonic.log
MAIN=/home/pi/ams-main/logs/main.log
WEB=/home/pi/ams-main/logs/web-client.log

tail -f $CORE $FLIGHT $LIDAR $SONIC $MAIN $WEB