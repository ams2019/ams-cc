#!/bin/bash

SERVICE="brcm_patchram_plus"

# Wait for service to start
until pids=$SERVICE
do
    sleep 1
done

# service has now started.

# There could be multiple service processes, so loop over the pids
for pid in $pids
do
    # Cleanup CPU hungry process
    sudo pkill -f $SERVICE
done

# The service process(es) have now been handled