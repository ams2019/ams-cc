#!/bin/bash

# Start Status LED
echo "starting service led"
sudo service led start

# Start ROScore
echo "starting service core"
sudo service core start
sleep 5

# Start ROSflight
echo "starting service flight"
sudo service flight start
sleep 5

# Start RPLidar
echo "starting service lidar"
sudo service lidar start

# Start Ultrasonic
echo "starting service sonic"
sudo service sonic start

# Perform Cleanup
echo "stop service brcm_patchram_plus"
#sudo /home/pi/ams-cc/npi/cleanup.sh
sudo service brcm_patchram_plus stop

# Start Main Code
echo "starting service main"
sudo service main start
sleep 5

# Start Web Interface
echo "starting service web"
sudo service web start

echo "all services started"