#!/bin/bash

echo "stopping service web"
sudo service web stop

echo "stopping service main"
sudo service main stop

echo "stopping service sonic"
sudo service sonic stop

echo "stopping service lidar"
sudo service lidar stop

echo "stopping service flight"
sudo service flight stop

echo "stopping service core"
sudo service core stop

echo "all services stopped"