#!/bin/bash

# ROS
source /opt/ros/melodic/setup.bash
export ROS_MASTER_URI=http://localhost:11311
export ROS_HOSTNAME=localhost

# ROSflight
source /home/pi/catkin_ws/devel/setup.bash
LOG=/home/pi/ams-cc/logs/rosflight.log
truncate -s 0 $LOG
unbuffer roslaunch /home/pi/ams-cc/rosflight.launch 2>&1 | tee $LOG