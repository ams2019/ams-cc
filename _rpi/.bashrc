# ROS
source /opt/ros/melodic/setup.bash
export ROS_MASTER_URI=http://localhost:11311
export ROS_HOSTNAME=localhost

# ROSflight
source ~/catkin_ws/devel/setup.bash
sudo chmod 666 /dev/ttyS0